package Pricing;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.Select;
import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;

public class PricingManager {
	private PhantomJSDriver ffx;
	
	public PricingManager(){
		DesiredCapabilities caps = new DesiredCapabilities();
	    caps.setJavascriptEnabled(true); 
	    caps.firefox();
	    ffx = new PhantomJSDriver(caps);
	}
	public void connect(){
		// info carsrent
		String siteName = "http://gmlithuania.fusemetrix.com/index.php";

		try {
			
			ffx.get(siteName);	// connect
			Cookie cookie = (Cookie) ffx.manage().getCookies().iterator().next();//set connection cookie
			ffx.manage().addCookie(cookie);

			WebElement username = ffx.findElement(By.id("username"));	//enter username
			username.sendKeys("info");
			
			WebElement password = ffx.findElement(By.id("password"));	//enter password
			password.sendKeys("carsrent");
			
			WebElement login = ffx.findElement(By.xpath("/html/body/div[2]/div[3]/div[1]/form[1]/input[5]"));
			login.click();	//click login button
			
			
		} catch (FailingHttpStatusCodeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	

	}
	public void changeFranchise(String franchise){
		ffx.get("http://gmlithuania.fusemetrix.com/bespoke/overrideprice.php?frommenu=true&f=1394790500");//Price override link http://gmlithuania.fusemetrix.com/home/loginHandler.php?logout=true
		ffx.switchTo().frame(2);//Switch to price override
		
		WebElement selectCity = ffx.findElement(By.id("vehicle_override_location_id"));//choose VILNIUS INTERNATIONAL AIRPORT
		new Select(selectCity).selectByVisibleText(franchise);	
	}
	public void chooseCategory(String carCategory){
		List<WebElement> myrowList = ffx.findElements(By.id("myrow"));//all categories list e.g Toyota Yaris or Similar
		Set<String> categories = new TreeSet<String>();
		for(WebElement e : myrowList){
			List<WebElement> myrowElemList = e.findElements(By.tagName("td")); 
//			System.out.println("\"" + myrowElemList.get(1).getText() + "\"");

			categories.add(myrowElemList.get(1).getText());
			
			if(myrowElemList.get(1).getText().equals(carCategory) && myrowElemList.get(3).getText().equals("9/9/2016 10:00")){
				
				WebElement editButton = e.findElement(By.tagName("a"));
				String editButtonStr = editButton.getText();
				System.out.println("a tags found " + editButtonStr);
				
				if(editButtonStr.equals("[edit...]")){
					editButton.click();// if edit found, of  choosen carCategory
					
//					break;
				}else{
					System.out.println("EDIT NOT FOUND");
					PrintWriter writer;
					try {
						writer = new PrintWriter("output.html", "UTF-8");
						writer.println(ffx.getPageSource());
						writer.close();
						
						ffx.close();
					} catch (FileNotFoundException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (UnsupportedEncodingException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					
				}
			}
		}
		for(String temp : categories){
			System.out.println("\"" + temp + "\"");
		}
		
	}
	public void changeDateFrom(int year, int month, int day){
//		System.out.println(ffx.findElement(By.id("vehiclegroupid")).getTagName());
		if(!ffx.findElements(By.id("vehiclegroupid")).isEmpty()){
			System.out.println("vehicle group id false");
			WebElement d_datefrom = ffx.findElement(By.id("d_dtmFrom"));
			d_datefrom.clear();
			d_datefrom.sendKeys("" + day);
			
			WebElement m_datefrom = ffx.findElement(By.id("m_dtmFrom"));
			m_datefrom.clear();
			m_datefrom.sendKeys("" + month);
			
			WebElement y_datefrom = ffx.findElement(By.id("y_dtmFrom"));
			y_datefrom.clear();
			y_datefrom.sendKeys("" + year);
		}else{
			System.out.println("vehicle group id true");
			
			WebElement d_datefrom = ffx.findElement(By.id("d_datefrom"));
			d_datefrom.clear();
			d_datefrom.sendKeys("" + day);
			
			WebElement m_datefrom = ffx.findElement(By.id("m_datefrom"));
			m_datefrom.clear();
			m_datefrom.sendKeys("" + month);
			
			WebElement y_datefrom = ffx.findElement(By.id("y_datefrom"));
			y_datefrom.clear();
			y_datefrom.sendKeys("" + year);
		}
	}
	public void changeDateTo(int year, int month, int day){
		if(ffx.findElement(By.id("vehiclegroupid")).getTagName().equals("select")){
			System.out.println("vehicle group id false");
			WebElement d_datefrom = ffx.findElement(By.id("d_dtmTo"));
			d_datefrom.clear();
			d_datefrom.sendKeys("" + day);
			
			WebElement m_datefrom = ffx.findElement(By.id("m_dtmTo"));
			m_datefrom.clear();
			m_datefrom.sendKeys("" + month);
			
			WebElement y_datefrom = ffx.findElement(By.id("y_dtmTo"));
			y_datefrom.clear();
			y_datefrom.sendKeys("" + year);
		}else{
			WebElement d_dateto = ffx.findElement(By.id("d_dateto"));
			d_dateto.clear();
			d_dateto.sendKeys("" + day);
			
			WebElement m_dateto = ffx.findElement(By.id("m_dateto"));
			m_dateto.clear();
			m_dateto.sendKeys("" + month);
			
			WebElement y_dateto = ffx.findElement(By.id("y_dateto"));
			y_dateto.clear();
			y_dateto.sendKeys("" + year);
		}
	}
	public void save(){
		WebElement saveBtn = ffx.findElement(By.xpath("/html/body/a[2]"));
		saveBtn.click();
		
	}
	
	public String getNextRowName(String col, int index){
		
		switch (col){
			case "rate":
				return "rate" + index;
			case "price[JIG][rate":
				return "price[JIG][rate" + index + "]";
			case "price[CTR][rate":
				return "price[CTR][rate" + index + "]";
			default :
				throw new IllegalArgumentException("Invalid row exception");
		}
	}
	public void changePrices(ArrayList<Float> priceDiffs, String rate){
		
		
//		String baseRate = "rate" + i;/*change prices of all days*/
//		String priceJIG = "price[JIG][rate" + i + "]";
//		String priceCRT = "price[CTR][rate" + i + "]";
		for(int i = 1; i < priceDiffs.size() + 1 && i < 31 ; i++){
			System.out.println(getNextRowName(rate, i));
			WebElement priceCell = ffx.findElement(By.name(getNextRowName(rate, i)));
			
			
			System.out.println(priceCell.getAttribute("value").toString());
			Float price = priceDiffs.get(i-1);
			if(price != null){
				if(i <= 3){
					price -= price * 0.35f;
				}else{
					price -= price * 0.2f;
				}
				System.out.println(price);
				priceCell.clear();
				priceCell.sendKeys("9999");
				
			}
			System.out.println("Rows found: " + priceCell.getAttribute("value"));
		}
	}
	public void duplicateCategory(String category){
		List<WebElement> myrowList = ffx.findElements(By.id("myrow"));//all categories list e.g Toyota Yaris or Similar
		
		for(WebElement e : myrowList){
			List<WebElement> myrowElemList = e.findElements(By.tagName("td")); 
//			System.out.println("\"" + myrowElemList.get(1).getText() + "\"");
			
			if(myrowElemList.get(1).getText().equals(category)){
				WebElement editButton = e.findElement(By.tagName("a"));
				String editButtonStr = editButton.getText();
				System.out.println("a tags found " + editButtonStr);
				
				if(editButtonStr.equals("[edit...]")){
					editButton.click();// if edit found, of  choosen carCategory
					
					
					WebElement duplicate = ffx.findElement(By.id("topbutton_audit"));
					duplicate.click();
					
					break;
				}else{
					System.out.println("EDIT NOT FOUND");
					PrintWriter writer;
					try {
						writer = new PrintWriter("output.html", "UTF-8");
						writer.println(ffx.getPageSource());
						writer.close();
						
						ffx.close();
					} catch (FileNotFoundException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (UnsupportedEncodingException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
			}
		}
	}
	public void close(){
		
		ffx.quit();
	}
	public void logOut(){
		
		ffx.get("http://gmlithuania.fusemetrix.com/home/loginHandler.php?logout=true");//log out link
		
	}
};
