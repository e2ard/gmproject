package Pricing;

public class PricingConstants {
	public final static String[] franchise = {"Vilnius International Airport, Vilnius Downtown, Vilnius Train Station ",
		"Riga Airport, Riga Train Station, Riga Bus Station, Riga Sea Port, Riga Downtown"};
	
	public final static String[] categories = {"Citroen Jumper 9 Seater or Similar",
		"Intermediate, automatic, with air conditioning, 4/5 doors. Volkswagen Jetta.",
		"Renault Traffic Passenger Van or Similar","Skoda Octavia, Volkswagen Passat, Wagon/Estate or similar",
		"Skoda Octavia/Volkswagen Passat","Toyota Rav 4, Automatic or similar","Toyota Rav 4, Manual or similar",
		"Toyota Yaris or Similar","Volkswagen Golf (AUTO) or Similar","Volkswagen Golf Wagon/Estate or Similar",
		"Volkswagen Golf or Similar","Volkswagen Passat (AUTO)","Volkswagen Passat or Similar",
		"Volkswagen Polo or Similar","Volkswagen Polo, Automatic or similar","Volkswagen Touran or Similar"};
	
	
}
