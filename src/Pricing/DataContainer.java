package Pricing;

import java.util.ArrayList;
import java.util.HashMap;

import Source.Offer;
import Source.Sites;

public class DataContainer {
	private static DataContainer instance = null;
	public HashMap<String, ArrayList<Float>> map = new HashMap<String, ArrayList<Float>>();
	
	public static DataContainer getIntance(){
	    if(instance == null) {
	    	instance = new DataContainer();
        }
		return instance;
	}
	
	public HashMap<String, ArrayList<Float>> getMap(){
		return map;
	}
	
	private void initMap(){
		for(String str : Sites.sPdfClasses){
			if(map.get(str) == null){
				map.put(str, new ArrayList<Float>());
			}
		}
	}
	
	public void addRow(ArrayList<Offer> offers){
		boolean isInitialized = false;
		if(!isInitialized){
			initMap();
		}
		Float priceDif;
		for(int i = 1; i < offers.size(); i++){
			if(offers.get(i).getSupplier() != null){
				priceDif = offers.get(i).getPrice();
				map.get(Sites.sPdfClasses[i]).add((priceDif*100.0f)/100.0f);
			}else{
				map.get(Sites.sPdfClasses[i]).add(null);
			}
		}
	}
	
	public ArrayList<Float> getDifs(String category){
		return map.get(category);
	}
}
