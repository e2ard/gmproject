package GUI;

import java.awt.Dimension;
import java.awt.event.*;
import java.util.Date;

import javax.swing.*;

import Source.Calendar;
import Source.Sites;
import Source.StatisticGenerator;

public class GuiDMain extends JFrame{
	
	private static final long serialVersionUID = -2082171821733818720L;
	
	private JComboBox<?> dayFromList;
	private JComboBox<?> monthFromList;
	private JComboBox<?> cityList;
	JTextArea titleField;
	JTextArea city;
	JTextArea dayFrom;
	JTextArea dayTo;
	JTextArea monthFrom;
	JTextArea daysToCount;
	JTextArea dayNum;

	JButton run;
	JButton cancel;
	Date date = new Date();
	protected int dayFromNum;
	protected int dayToNum;
	protected int puMonthNum;
	protected int daysToCountNum;
	protected String cityName;
	protected int daysNum;
	protected boolean isRunning = false;
    
	String months[] = {"1"," 2"," 3"," 4"," 5"," 6"," 7"," 8"," 9"," 10"," 11"," 12"};
	String cityListStr[] = {"Vilnius", "Riga"};
	
   public GuiDMain()
   {
	   getContentPane().setLayout(null);
	   date.setDate(date.getDate() + 1);
	   setupGUI();
	   setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
   }
   @SuppressWarnings({ "unchecked", "rawtypes" })
void setupGUI(){
 	

	titleField = new JTextArea();
	titleField.setLocation(11,7);
	titleField.setSize(450,76);
	titleField.setLineWrap(true);
	titleField.setText("Rental car analytic system, choose date (optional) "
			+ "and city.\nPdf will be placed in Pdf folder (must be placed in the same dir where app is). ");
	titleField.setRows(5);
	titleField.setColumns(5);
	titleField.setEditable(false);
	getContentPane().add(titleField);

	cityList = new JComboBox(cityListStr);
	cityList.setLocation(360,96);
	cityList.setSize(100,20);
	cityList.setEditable(false );
//	cityList.setEnabled(false);
	cityList.setSelectedItem(null);
	cityList.addActionListener (new ActionListener () {
	    public void actionPerformed(ActionEvent e) {
	        System.out.println(cityList.getSelectedItem().toString());
	        cityName = cityList.getSelectedItem().toString();
	        setCityName(cityName);
	    }
	});
	getContentPane().add(cityList);

	dayFrom = new JTextArea();
	dayFrom.setLocation(9,96);
	dayFrom.setSize(65,20);
	dayFrom.setText(" Day from");
	dayFrom.setRows(5);
	dayFrom.setColumns(5);
	getContentPane().add(dayFrom);

	dayFromList = new JComboBox(Sites.daysStr);
	dayFromList.setLocation(85,96);
	dayFromList.setSize(50,20);

	Date date = new Date();
	dayFromList.setSelectedItem(Sites.daysStr[date.getDate()]);
	dayFromNum = dayFromList.getSelectedIndex() + 1;
	dayFromList.addActionListener (new ActionListener () {
	    public void actionPerformed(ActionEvent e) {
	    	dayFromNum = dayFromList.getSelectedIndex() + 1;
	    	System.out.println(dayFromNum);
	    	setDayFromNum(dayFromNum);
	    }
	});
	dayFromList.setEditable(false);
	getContentPane().add(dayFromList);

//	dayTo = new JTextArea();
//	dayTo.setLocation(9,134);
//	dayTo.setSize(65,20);
//	dayTo.setText(" Days to count");
//	dayTo.setRows(5);
//	dayTo.setColumns(5);
//	getContentPane().add(dayTo);

//	dayToList =new JComboBox(Sites.daysStr);
//	dayToList.setLocation(85,134);
//	dayToList.setSize(50,20);
//	dayToList.setEditable(false);
//	dayToList.setSelectedItem(Sites.daysStr[1]);
//	dayToList.addActionListener (new ActionListener () {
//	    public void actionPerformed(ActionEvent e) {
//	    	dayToNum = dayToList.getSelectedIndex() + 1;
//	    	System.out.println(dayToNum);
//	    	setDayToNum(dayToNum);
//	    }
//	});
//    getContentPane().add(dayToList);
//
	
	monthFromList = new JComboBox(months);
	monthFromList.setLocation(224,96);
	monthFromList.setSize(50,20);
	monthFromList.setEditable(false );
	
//	monthFromList.setSelectedItem(months[10]);
	monthFromList.setSelectedItem(months[date.getMonth()]);
	
	
	
	puMonthNum = monthFromList.getSelectedIndex() + 1;
	monthFromList.addActionListener (new ActionListener () {
	    public void actionPerformed(ActionEvent e) {
	    	puMonthNum = monthFromList.getSelectedIndex() + 1;
	    	System.out.println(puMonthNum);
	    	setPuMonth(puMonthNum);
	    }
	});
    getContentPane().add(monthFromList);

//	daysToCount = new JTextArea();
//	daysToCount.setLocation(100,134);
//	daysToCount.setSize(115,20);
//	daysToCount.setText("Days to count");
//	daysToCount.setRows(5);
//	daysToCount.setColumns(5);
//	getContentPane().add(daysToCount);
	
	monthFrom = new JTextArea();
	monthFrom.setLocation(144,96);
	monthFrom.setSize(70,20);
	monthFrom.setText("Month from");
	monthFrom.setRows(5);
	monthFrom.setColumns(5);
	getContentPane().add(monthFrom);

//	daysToCountList = new JComboBox(Sites.daysStr);
//	daysToCountList.setLocation(224,134);
//	daysToCountList.setSize(50,20);
//	daysToCountList.setEditable(false );
//	daysToCountList.setSelectedItem(months[0]);
//	daysToCountList.addActionListener (new ActionListener () {
//	    public void actionPerformed(ActionEvent e) {
//	        System.out.println(daysToCountList.getSelectedIndex() + 1 );
//	        daysToCountNum = daysToCountList.getSelectedIndex() + 1;
//	        setDaysNum(daysToCountNum);
//	    }
//	});
//	getContentPane().add(daysToCountList);

//	daysNumList = jComboBox;
//	daysNumList.setLocation(198,191);
//	daysNumList.setSize(50,20);
//	daysNumList.setEditable(false );
//	daysNumList.setSelectedItem(Sites.daysStr[5]);
//	
//	daysNumList.addActionListener (new ActionListener () {
//	    public void actionPerformed(ActionEvent e) {
//	       System.out.println(daysNumList.getSelectedIndex() + 1 );
//	       setDayNum(daysNumList.getSelectedIndex() + 1);
//	    }
//	});
//	getContentPane().add(daysNumList);

	city = new JTextArea();
	city.setLocation(280,96);
	city.setSize(79,20);
	city.setText("City* ");
	city.setRows(5);
	city.setColumns(5);
	getContentPane().add(city);
	
//	dayNum = new JTextArea();
//	dayNum.setLocation(11,191);
//	dayNum.setSize(178,20);
//	dayNum.setText("Number of days per document");
//	dayNum.setRows(5);
//	dayNum.setColumns(5);
//	getContentPane().add(dayNum);

	run = new JButton();
	run.setLocation(250,217);
	run.setSize(100,30);
	run.setText("Run");
	run.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent evt) {
			long startTime = System.currentTimeMillis();
			
			int puDate  = puMonthNum * 100 + dayFromNum;
			int sysDate = date.getMonth() * 100 + date.getDate();
			
			if(isZero(dayFromNum) && isZero(puMonthNum) && cityName!=null && (puDate > sysDate)){
		    	
		    	System.out.println("-->Main started");
				System.out.println("Run pressed");
		    	
		    	System.out.println("Values puDay " + dayFromNum + " doDay" + dayToNum + "puMonth" + puMonthNum + " " + daysToCountNum) ;
		    	System.out.println("daysNum" + daysNum);
//		    	String rentalRiga = "http://www.rentalcars.com/SearchResults.do;jsessionid=3CC534923D8DCCD13A0EAD109774652A.node241a?dropCity=Ryga&doMinute=0&location=1373298&driversAge=25&doHour=10&filterName=CarCategorisationSupplierFilter&searchType=&locationName=Ryga+Oro+Uostas&doMonthYear=10-2015&puSameAsDo=on&city=Ryga&puMonthYear=10-2015&chinese-license=on&dropCountry=Latvija&puDay=20&filterTo=1000&dropLocation=1373298&doDay=22&dropLocationName=Ryga+Oro+Uostas&enabler=&country=Latvija&filterFrom=0&puMonth=10&puMinute=0&doMonth=10&doYear=2015&puYear=2015&filter_carclass=economy&filterAdditionalInfo=&advSearch=&exSuppliers=&ordering=recommended";
				String rentalRiga =    "http://www.rentalcars.com/SearchResults.do;jsessionid=DE2574FFEFE06FF009AF4FAAA2368A0E.node241a?&dropCity=Riga&doMinute=0&cor=fr&location=388021&driversAge=25&exSuppliers=&doHour=10&locationName=Riga+Airport+%28RIX%29&city=R%C4%ABga&page=SearchResults.do&puHour=10&preflang=en&dropCountry=Latvia&affiliateCode=airbaltic&puDay=20&dropLocation=388021&doDay=22&dropLocationName=Riga+Airport+%28RIX%29&country=Latvia&filter_carclass=economy&filterAdditionalInfo=&advSearch=&puMonth=9&puMinute=0&doMonth=9&doYear=2015&puYear=2015&filterTransmission=Manual";
				String rentalVilnius = "http://www.rentalcars.com/SearchResults.do;jsessionid=DE2574FFEFE06FF009AF4FAAA2368A0E.node226a?&dropCity=Vilnius&doMinute=0&cor=fr&location=14159&driversAge=25&exSuppliers=&doHour=10&locationName=Vilnius+Airport&city=Vilnius&page=SearchResults.do&puHour=10&preflang=en&dropCountry=Lithuania&affiliateCode=airbaltic&puDay=12&dropLocation=14159&doDay=22&dropLocationName=Vilnius+Airport&country=Lithuania&filter_carclass=economy&filterAdditionalInfo=&advSearch=&puMonth=9&puMinute=0&doMonth=9&doYear=2015&puYear=2015&filterTransmission=Manual";
				String rental;
				
//				cityName = "Riga";
//				daysToCountNum = 1;
//				dayFromNum =6;
//				puMonthNum = 10;
				if(isRunning != true){
					isRunning = true;
					if(cityName.contains("Vilnius")){
						rental = rentalVilnius;
					}else{
						rental = rentalRiga;
					}
					//init date
					Calendar cal = Calendar.getIntance();
					cal.setPuDay(dayFromNum);
					cal.setPuMonth(puMonthNum);
					cal.setPuYear(2015);
					
			    	Thread messege = new Thread(new Runnable(){
				        public void run(){
				        	JOptionPane.showMessageDialog(null, "Please, wait. Calculaiting!", "Warrning", JOptionPane.CLOSED_OPTION);
				                
				        }
				    });
				    messege.start();
			    
			    	Thread calculations = new Thread(new Runnable(){
			    		public void run(){
			            	StatisticGenerator sr = new StatisticGenerator();
//							sr.getPdfFast(rental);
							isRunning = false;
			        	}
			    	});
			    	calculations.start();
		    	
				}else{
			    	Thread messege = new Thread(new Runnable(){
				        public void run(){
				        	JOptionPane.showMessageDialog(null, "Please, wait. Already calculaiting!!!", "Warrning", JOptionPane.CLOSED_OPTION);
				        }
				    });
				    messege.start();
				}

				long endTime = System.currentTimeMillis();
				System.out.println("-->Main done" + "\n" + (endTime - startTime) / 1000);
			}else{
				JOptionPane.showMessageDialog(null, "Enter all fields correctly.");
			}
		}
	});
	getContentPane().add(run);

	cancel = new JButton();
	cancel.setLocation(365,217);
	cancel.setSize(100,30);
	cancel.addActionListener(new ActionListener() {
	      @Override
	      public void actionPerformed(ActionEvent evt) {
	           System.exit(0);
	      }
	});
	cancel.setText("Cancel");
	getContentPane().add(cancel);
	setTitle("Price comparison");
	setSize(556,502);
	setVisible(true);
	setResizable(true);
	
	this.setMinimumSize(new Dimension(500, 300));
    this.pack();
    this.setResizable(false);
   }
	
//	void setupGUI()
//	{	
//		monthToList = new JComboBox(Sites.daysStr);
//		monthToList.setLocation(224,134);
//		monthToList.setSize(50,20);
//		monthToList.setEditable(false );
//		monthToList.addActionListener (new ActionListener () {
//			
//			
//			public void actionPerformed(ActionEvent e) {
//		        System.out.println(monthToList.getSelectedIndex() + 1 );
//		        monthTo = monthToList.getSelectedIndex() + 1;
//		    }
//		});
//		Main.puMonth = monthTo;
//   	
//		ComboBoxes cb = new ComboBoxes();
//		getContentPane().add(cb.dayFromList());
//		getContentPane().add(cb.dayToList());
//		getContentPane().add(cb.monthToList());
//		getContentPane().add(cb.monthFromList());
//		getContentPane().add(cb.daysNumList());
//		getContentPane().add(cb.cityList());
//		
//		TextAreas ta = new TextAreas();
//		getContentPane().add(ta.title());
//		getContentPane().add(ta.monthFrom());
//		getContentPane().add(ta.monthTo());
//		getContentPane().add(ta.dayNum());
//		getContentPane().add(ta.dayFrom());
//		getContentPane().add(ta.dayTo());
//		getContentPane().add(ta.dayTo());
//		getContentPane().add(ta.city());
//		
//		Buttons but = new Buttons();
//		getContentPane().add(but.run());
//		getContentPane().add(but.cancel());
//		
//		setTitle("Price Comparison");
//		setSize(556,502);
//		setVisible(true);
//		setResizable(true);
//		
//	}
//	public ComboBoxes getComboBoxes(){
//		return ;
//	}
   	public void setDayNum(int num){
   		daysNum = num;
   	}
	private void setDayFromNum(int dayFromNum) {
		this.dayFromNum = dayFromNum;
	}
	
	private void setPuMonth(int monthFromNum) {
		this.puMonthNum = monthFromNum;
	}
	private void setDaysNum(int monthToNum) {
		this.daysToCountNum = monthToNum;
	}
	private void setCityName(String cityName) {
		this.cityName = cityName;
	}
	
	private boolean isZero(int num){
		return num > 0?true:false;
	}
	public static void main( String args[] ) {
	  
	  new GuiDMain();
	}
	
};  
