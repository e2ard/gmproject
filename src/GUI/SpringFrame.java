package GUI;

import javax.swing.SpringLayout;

import GUI.ProgressBarD.Task;

import org.jdatepicker.impl.JDatePanelImpl;
import org.jdatepicker.impl.JDatePickerImpl;
import org.jdatepicker.impl.UtilDateModel;















import Source.Sites;
import Source.Calendar;
import Source.StatisticGenerator;

import javax.swing.JButton;
import javax.swing.JCheckBox;

import GUI.ComboBoxesColored;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;
import javax.swing.SwingWorker;
import javax.swing.UIManager;

import java.awt.Color;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;
import java.util.Properties;
import java.util.Random;
import java.util.concurrent.ExecutionException;
import java.awt.Cursor;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.net.URL;

import GUI.ProgressBarD;
 
public class SpringFrame {
    /**
     * Create the GUI and show it.  For thread safety,
     * this method should be invoked from the
     * event-dispatching thread.
     */
    JCheckBox loadPrices;
    private static int sysDate;
    private static int daysToCompareNum;
    private static String cityName;
    private static String source;
    private static int daysNum;
    private static boolean isRunning = false;
    private static Task task;
    static JComponent progressBar;
    private  JButton runBtn;
    public static class Task extends SwingWorker<Void, Void> {
        /*
         * Main task. Executed in background thread.
         */
        @Override
        public Void doInBackground() {
            Random random = new Random();
            //Initialize progress property.
            setProgress(0);
            while (Sites.progress < 100) {
                //Sleep for up to one second.
                try {
                    Thread.sleep(2500);
                } catch (InterruptedException ignore) {}
                //Make random progress.
//                progress += random.nextInt(10);
                System.out.println("------------>" + Sites.progress);
                setProgress(Math.min(Sites.progress, 100));
            }
            return null;
        }

        /*
         * Executed in event dispatching thread
         */
        @Override
        public void done() {
            task.cancel(true);
            Sites.progress = 100;
            Toolkit.getDefaultToolkit().beep();
        }
    }
    private static  void createAndShowGUI() {
        //Create and set up the window.
        JFrame frame = new JFrame("PriceComparison v0.17.5");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
 
        //Set up the content pane.
        Container contentPane = frame.getContentPane();
        SpringLayout layout = new SpringLayout();
        contentPane.setLayout(layout);
        contentPane.setBackground(Color.WHITE);
        //text intro
        JTextArea textIntro = new JTextArea(" Rental car analytic system, choose date (optional) "
            + "and city.\n Pdf will be placed in Pdf folder (must be placed in the same dir where app is). ");
        textIntro.setEditable(false);           
        textIntro.setPreferredSize(new Dimension(425, 50));
        JLabel puDateLabel = new JLabel(" Pick-up date: ");
       
        // date picker
        UtilDateModel model = new UtilDateModel();
        sysDate = model.getYear() * 10000 + (model.getMonth() + 1) * 100 + model.getDay();
        model.setDate(model.getYear(), model.getMonth(), model.getDay() + 1);
       
        model.setSelected(true);
       
        Properties p = new Properties();
        p.put("text.today", "Todays");
        p.put("text.month", "Months");
        p.put("text.year", "Years");
      
        JDatePanelImpl datePanel = new JDatePanelImpl(model, p);
        datePanel.setForeground(new Color(255, 251, 250));
        JDatePickerImpl datePicker = new JDatePickerImpl(datePanel, new DateLabelFormatter());
       
        URL url = SpringFrame.class.getResource(
                "/icons/datepicker_icon.jpg");
        ImageIcon icon = new ImageIcon(url);
       
        UIManager.put("Button.background", Sites.greenBg);
        UIManager.put("Button.foreground", Sites.greenFg);
       
        JButton pickerBtn = (JButton)datePicker.getComponent(1);
       
        pickerBtn.setBackground(Color.white);
        pickerBtn.setForeground(Color.white);
       
        pickerBtn.setContentAreaFilled(false);
        pickerBtn.setBorder(BorderFactory.createEmptyBorder());
        pickerBtn.setMargin(new Insets(60,0,30,60));
       
        pickerBtn.setFont(new Font("Tahoma", Font.BOLD, 0));
        pickerBtn.setIcon(icon);
      
        datePicker.setPreferredSize(new Dimension(110, 25));
        datePicker.getJFormattedTextField().setForeground(Sites.greenFg);
        datePicker.getJFormattedTextField().setBackground(Sites.greenBg);
        datePicker.getJFormattedTextField().setFont(Sites.font
        		);
       
        // combo box of source
        ComboBoxesColored sourceCombo = new ComboBoxesColored(Sites._Sources);
        sourceCombo.setSelectedIndex(0);
        source = sourceCombo.getSelectedItem().toString();
        sourceCombo.addActionListener (new ActionListener () {
            public void actionPerformed(ActionEvent e) {
                System.out.println(sourceCombo.getSelectedItem().toString());
                source = sourceCombo.getSelectedItem().toString();
            }
        });
        //Source label
        JLabel sourceLabel = new JLabel(" Suource: ");
        // city label     
        JLabel cityLabel = new JLabel(" City: ");
        // city combobox
        ComboBoxesColored cityCombo = new ComboBoxesColored(Sites._Cities);
        cityCombo.setSelectedIndex(0);
        cityName = cityCombo.getSelectedItem().toString();
        cityCombo.addActionListener (new ActionListener () {
            public void actionPerformed(ActionEvent e) {
                System.out.println(cityCombo.getSelectedItem().toString());
                cityName = cityCombo.getSelectedItem().toString();
            }
        });
        // city label     
        JLabel adjustLabel = new JLabel(" Adjust to: ");
        // city combobox
        ComboBoxesColored adjustCombo = new ComboBoxesColored(Sites._Adjust);
        adjustCombo.setEnabled(false);
        // percents label     
        JLabel lessLabel = new JLabel(" Less by : ");
        // percents combobox
        ComboBoxesColored lessCombo = new ComboBoxesColored(Sites.daysStr);
        lessCombo.setEnabled(false);
       
        // city label     
        JLabel daysToCompareLabel = new JLabel(" Days to compare: ");
        //check box load prices
        JCheckBox loadPrices = new JCheckBox(" Load Prices: ");
        loadPrices.setBackground(Color.white);
        loadPrices.setHorizontalTextPosition(SwingConstants.LEFT);
        loadPrices.setSelected(false);
        loadPrices.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent evt) {
                if(loadPrices.isSelected()){
                    lessCombo.setEnabled(true);
                    adjustCombo.setEnabled(true);
                }else{
                    lessCombo.setEnabled(false);
                    adjustCombo.setEnabled(false);
                }
            }
        });
        // city combobox
        ComboBoxesColored daysToCompareCombo = new ComboBoxesColored(Sites.daysStr);
        daysToCompareCombo.setSelectedItem(Sites.daysStr[Sites.daysStr.length / 2]);
        daysToCompareCombo.setMaximumRowCount(Sites.daysStr.length);
        daysToCompareNum = Integer.parseInt(Sites.daysStr[daysToCompareCombo.getSelectedIndex()].trim());
        daysToCompareCombo.addActionListener (new ActionListener () {
            public void actionPerformed(ActionEvent e) {
                daysToCompareNum = Integer.parseInt(Sites.daysStr[daysToCompareCombo.getSelectedIndex()].trim());
                System.out.println(daysToCompareNum);
            }
        });
        // button run
        JButton runBtn = new JButton("Run");
        runBtn.setFont(Sites.font);
        class TaskListener implements PropertyChangeListener {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
               if ("progress".equals(evt.getPropertyName())) {
                  int progress = task.getProgress();
                 
                  ((ProgressBarD) progressBar).setValue(Sites.progress);
               }
               if (SwingWorker.StateValue.DONE == evt.getNewValue()) {
                  // always need to know when the SW is done
                  // so we can call get() and trap exceptions
                  try {
                     task.get();
                  } catch (InterruptedException | ExecutionException e) {
                     e.printStackTrace();
                  }
               }
            }
         }
        runBtn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                int puDay = datePicker.getModel().getDay();
                int puMonth = datePicker.getModel().getMonth() + 1;
                int puYear = datePicker.getModel().getYear();
                int puDate = puYear * 10000 + puMonth * 100 + puDay;
                String rental, norwegian, scanner;
                long startTime = System.currentTimeMillis();
                System.out.println(puDate + " " + puMonth + " " + sysDate);
                if(isRunning == false && (puDate > sysDate) && source != null && cityName != null && daysToCompareNum > 0){
                    isRunning = true;
                    runBtn.setEnabled(false);
                    Sites.progress = 0;
                    task = new Task();
                    task.addPropertyChangeListener(new TaskListener() );
                    task.execute();
                    contentPane.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                    //Instances of javax.swing.SwingWorker are not reusuable, so
                    //we create new instances as needed.
                   
                   
                    if(cityName.contains("Vilnius")){
                        rental = Sites.rentalVilnius;
                        norwegian = Sites.norwegianVilnius;
                        scanner = Sites.scannerVilnius;
                    }else{
                        rental = Sites.rentalRiga;
                        norwegian = Sites.norwegianRiga;
                        scanner = Sites.scannerRiga;
                    }
                    //init date
                    Calendar cal = Calendar.getIntance();
                    cal.setPuDay(puDay);
                    cal.setPuMonth(puMonth);
                    cal.setPuYear(2015);
                    // messege thread
                   
                    //----
                    //--Calculations thread
                    Thread calculations = new Thread(new Runnable(){
                        public void run(){
                            StatisticGenerator sr = new StatisticGenerator();
                            if(source.contains(Sites._Sources[0])){
                                System.out.println("Rentalcars");
                                sr.getPdfFast(rental, daysToCompareNum);
                            }else if(source.contains(Sites._Sources[1])){
                                System.out.println("Norwegian");
                                sr.getNorwegian(norwegian, daysToCompareNum);
                            }else if(source.contains(Sites._Sources[2])){
                            	 System.out.println("Scanner");
                                 sr.getCarScanner(scanner, daysToCompareNum);
                            }
                            isRunning = false;
                            runBtn.setEnabled(true);
                            Sites.progress = 100;
                            contentPane.setCursor(null);
                            JOptionPane.showMessageDialog(null, "Done", "Done", JOptionPane.CLOSED_OPTION);
                        }
                    });
                    calculations.start();
                    //-----
                }else
                    if(isRunning == true){
                    // wrong parameters messege thread
                    Thread messege = new Thread(new Runnable(){
                        public void run(){
                            JOptionPane.showMessageDialog(null, "Already calculaiting. Please, WAIT!!!", "Error", JOptionPane.ERROR_MESSAGE);

                        }
                     
                    });
                    messege.start();
                    //---
                }else{
                    Thread messege = new Thread(new Runnable(){
                        public void run(){
                            JOptionPane.showMessageDialog(null, "Enter all fields correctly.");
                        }
                    });
                    messege.start();
                }   
                long endTime = System.currentTimeMillis();
                System.out.println("-->Main done" + "\n" + (endTime - startTime) / 1000);
       
               
//                JOptionPane.showMessageDialog(null, "Please, wait. Calculaiting!" + cityName + " " + source  + " " +
//                daysToCompareNum + (puDate > sysDate? "OK" : "BAD"), "Warrning", JOptionPane.CLOSED_OPTION);
//               
            }
        });
       
        // button cancel
        JButton cancelBtn = new JButton("Cancel");
        cancelBtn.setFont(Sites.font);
        cancelBtn.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent evt) {
               System.exit(0);
          }
        });
       
        contentPane.add(textIntro);
        contentPane.add(datePicker);
        contentPane.add(puDateLabel);
        contentPane.add(sourceCombo);
        contentPane.add(sourceLabel);
        contentPane.add(cityLabel);
        contentPane.add(cityCombo);
        contentPane.add(adjustLabel);
        contentPane.add(adjustCombo);
        contentPane.add(daysToCompareLabel);
        contentPane.add(daysToCompareCombo);
        contentPane.add(lessLabel);
        contentPane.add(lessCombo);
//        contentPane.add(loadPrices);
        contentPane.add(runBtn);
        contentPane.add(cancelBtn);
       
        //puDate label
        layout.putConstraint(SpringLayout.WEST, puDateLabel,
                10,
                SpringLayout.WEST, contentPane);
        layout.putConstraint(SpringLayout.NORTH, puDateLabel,
                75,
                SpringLayout.NORTH, contentPane);
       
        // date picker
        layout.putConstraint(SpringLayout.WEST, datePicker,
                 5,
                 SpringLayout.EAST, puDateLabel);
        layout.putConstraint(SpringLayout.NORTH, datePicker,
                 60,
                 SpringLayout.NORTH, textIntro);
       
        //text intro
        layout.putConstraint(SpringLayout.WEST, textIntro,
                5,
                SpringLayout.WEST, contentPane);
        layout.putConstraint(SpringLayout.NORTH, textIntro,
                10,
                SpringLayout.NORTH, contentPane);
       
        //source bombobox
        layout.putConstraint(SpringLayout.EAST, sourceCombo,
                -6,
                SpringLayout.EAST,  textIntro);
        layout.putConstraint(SpringLayout.NORTH, sourceCombo,
                60,
                SpringLayout.NORTH, textIntro);
       
        //source label
        layout.putConstraint(SpringLayout.EAST, sourceLabel,
                -105,
                SpringLayout.EAST,  textIntro);
        layout.putConstraint(SpringLayout.NORTH, sourceLabel,
                63,
                SpringLayout.NORTH, textIntro);
       
      //city label
        layout.putConstraint(SpringLayout.EAST, cityLabel,
                -105,
                SpringLayout.EAST,  textIntro);
        layout.putConstraint(SpringLayout.NORTH, cityLabel,
                95,
                SpringLayout.NORTH, textIntro);
        //city combo
        layout.putConstraint(SpringLayout.WEST, cityCombo,
                8,
                SpringLayout.EAST,  cityLabel);
        layout.putConstraint(SpringLayout.NORTH, cityCombo,
                0,
                SpringLayout.NORTH, cityLabel);
        //days to count  label
        layout.putConstraint(SpringLayout.EAST, daysToCompareLabel,
                30,
                SpringLayout.WEST,  cityCombo);
        layout.putConstraint(SpringLayout.NORTH, daysToCompareLabel,
                65,
                SpringLayout.NORTH, cityCombo);
        //days to count combo
        layout.putConstraint(SpringLayout.EAST, daysToCompareCombo,
                0,
                SpringLayout.EAST,  cityCombo);
        layout.putConstraint(SpringLayout.NORTH, daysToCompareCombo,
                58,
                SpringLayout.NORTH, cityLabel);
       
        //adjust  label
        layout.putConstraint(SpringLayout.EAST, adjustLabel,
                -5,
                SpringLayout.WEST,  datePicker);
        layout.putConstraint(SpringLayout.NORTH, adjustLabel,
                135,
                SpringLayout.NORTH, datePicker);
       
        //adjust  combo
       
        layout.putConstraint(SpringLayout.WEST, adjustCombo,
                25,
                SpringLayout.NORTH, datePicker);
        layout.putConstraint(SpringLayout.SOUTH, adjustCombo,
                133,
                SpringLayout.SOUTH, datePicker);
        //loadPrices  checkbox
        layout.putConstraint(SpringLayout.EAST, loadPrices,
                -137,
                SpringLayout.WEST,  daysToCompareLabel);
        layout.putConstraint(SpringLayout.NORTH, loadPrices,
                -30,
                SpringLayout.NORTH, daysToCompareLabel);
      //less  combobox
        layout.putConstraint(SpringLayout.WEST, lessCombo,
                0,
                SpringLayout.WEST,  adjustCombo);
        layout.putConstraint(SpringLayout.NORTH, lessCombo,
                -4,
                SpringLayout.NORTH, daysToCompareLabel);
       
        //less  label
        layout.putConstraint(SpringLayout.EAST, lessLabel,
                0,
                SpringLayout.EAST,  adjustLabel);
        layout.putConstraint(SpringLayout.NORTH, lessLabel,
                0,
                SpringLayout.NORTH, daysToCompareLabel);
        // cancel button
        layout.putConstraint(SpringLayout.EAST, cancelBtn,
                -15,
                SpringLayout.EAST,  contentPane);
        layout.putConstraint(SpringLayout.SOUTH, cancelBtn,
                -12,
                SpringLayout.SOUTH, contentPane);
       
        // cancel button
        layout.putConstraint(SpringLayout.EAST, runBtn,
                -12,
                SpringLayout.WEST,  cancelBtn);
        layout.putConstraint(SpringLayout.SOUTH, runBtn,
                0,
                SpringLayout.SOUTH, cancelBtn);
       
       
        progressBar = new ProgressBarD();
        progressBar.setOpaque(true); //content panes must be opaque
       
        runBtn.addActionListener(new ActionListener(){

            public void actionPerformed(ActionEvent evt) {
               
            }
           
        });
       
       
        contentPane.add(progressBar);
     // cancel button
        layout.putConstraint(SpringLayout.EAST, runBtn,
                -15,
                SpringLayout.WEST,  cancelBtn);
        layout.putConstraint(SpringLayout.SOUTH, runBtn,
                0,
                SpringLayout.SOUTH, cancelBtn);
       
     // cancel button
        layout.putConstraint(SpringLayout.EAST, progressBar,
                120,
                SpringLayout.EAST,  adjustLabel);
        layout.putConstraint(SpringLayout.NORTH, progressBar,
                25,
                SpringLayout.NORTH, adjustLabel);
        //Display the window.
        frame.setMinimumSize(new Dimension(440, 300));
        frame.pack();
        frame.setVisible(true);
        frame.setResizable(false);
    }
   
   
    public static void main(String[] args) {
        //Schedule a job for the event-dispatching thread:
        //creating and showing this application's GUI.
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createAndShowGUI();
            }
        });
    }
};