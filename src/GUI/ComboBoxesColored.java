package GUI;

import java.awt.Color;

import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.UIManager;
import javax.swing.plaf.basic.BasicComboPopup;

import Source.Sites;

public class ComboBoxesColored extends JComboBox{
	public ComboBoxesColored(String[] args){
		super(args);
		UIManager.put( "ComboBox.disabledBackground", Sites.greenBg);
		UIManager.put( "ComboBox.disabledForeground", Color.GRAY);
		this.setBackground(Sites.greenBg);
		this.setForeground(Sites.greenFg);
		Object child = this.getAccessibleContext().getAccessibleChild(0);
		BasicComboPopup popup = (BasicComboPopup)child;
		JList list = popup.getList();
		list.setSelectionBackground(Sites.greenFg);
		list.setSelectionForeground(Sites.greenSelection);
		
	}

}
