package Source;
import java.awt.Color;
import java.awt.Font;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class Sites {
	private String siteName, pattern;
	private String fileName = "site";// file to read from
	private Calendar cal; // calendar of date management 
	
	public ArrayList<String> sites = new ArrayList<String>();//red from file - sites
	public ArrayList<String> sNames = new ArrayList<String>();//generated site list
	public static final String[] sPdfClasses = {"MDMR", "EDMR", "EDAR", "CDMR", "CDAR", "IDMR", "IDAR", "SDMR", "SDAR", "CJMR", "CJAR","SUV", "SUV A", "PVMR"};
	public static final String[] sClasses = {"mini", "economy", "compact", "intermediate", "standard", "estate", "suvs", "carriers_9"};
	public static final String[] sNorwegian = {"Mini", "Economym", "Economya", "Compactm", "Compacta", "Intermediatem", "Intermediatea", "Standardm", "Standarda", "Estatem", "Estatea", "SUVm", "SUVa", "9-seat minivanm"};
	public static final String[] sAirbaltic = {"Minim", "Economym", "Economya", "Compactm", "Compacta", "Intermediatem", "Intermediatea", "Standardm", "Standarda", "Estatem", "Estatea", "SUVm", "SUVa", "9-seat minivanm"};
	public static final String[] sAirbalticLt = {"Ekonominism", "Ekonominisa", "Kompaktinism", "Kompaktinisa", "Vidutinism", "Vidutinisa", "Standartinism", "Standartinisa", "Visureigism", "Visureigisa", "Mikroautobusasm"};
	public static final String[] sScanner = {"Minim", "Economym", "Economya", "Compactm", "Compacta", "Standard / intermediatem", "Standard / intermediatea", "Standard / intermediatem", "Standard / intermediatea", "Estatem", "Estatea", "SUVm", "SUVa", "9 seat minivanm"};
	public static final int[] days = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30};
//	public static final String[] daysStr = {" 1", " 2", " 3", " 4", " 5", " 6", " 7", " 8", " 9", " 10", " 11", " 12", " 13", " 14", " 15", " 16", " 17", " 18", " 19", " 20", " 21", " 22", " 23", " 24", " 25", " 26", " 27", " 28", " 29", " 30"};
//	public static final int[] days = {1, 2, 3, 4, 5, 6, 7, 8, 10, 12, 14, 16, 18, 22, 25, 30};
	public static final String[] daysStr = {" 1", " 2", " 3", " 4", " 5", " 6", " 7", " 8", " 10", " 12", " 14", " 16"};
	public static final String[] _Sources = {"RentalCars", "Norwegian", "Scanner"};
	public static final String[] _Cities = {"Vilnius        ", "Riga"};
	public static final String[] _Adjust = {"Base Rate", "JIG", "CRT"};
	// source links
	public static final String rentalRiga =    "http://www.rentalcars.com/SearchResults.do;jsessionid=DE2574FFEFE06FF009AF4FAAA2368A0E.node241a?&dropCity=Riga&doMinute=0&cor=fr&location=388021&driversAge=25&exSuppliers=&doHour=10&locationName=Riga+Airport+%28RIX%29&city=R%C4%ABga&page=SearchResults.do&puHour=10&preflang=en&dropCountry=Latvia&affiliateCode=airbaltic&puDay=20&dropLocation=388021&doDay=22&dropLocationName=Riga+Airport+%28RIX%29&country=Latvia&filter_carclass=economy&filterAdditionalInfo=&advSearch=&puMonth=9&puMinute=0&doMonth=9&doYear=2015&puYear=2015&filterTransmission=Manual";
	public static final String rentalVilnius = "http://www.rentalcars.com/SearchResults.do;jsessionid=DE2574FFEFE06FF009AF4FAAA2368A0E.node226a?&dropCity=Vilnius&doMinute=0&cor=fr&location=14159&driversAge=25&exSuppliers=&doHour=10&locationName=Vilnius+Airport&city=Vilnius&page=SearchResults.do&puHour=10&preflang=en&dropCountry=Lithuania&affiliateCode=airbaltic&puDay=12&dropLocation=14159&doDay=22&dropLocationName=Vilnius+Airport&country=Lithuania&filter_carclass=economy&filterAdditionalInfo=&advSearch=&puMonth=9&puMinute=0&doMonth=9&doYear=2015&puYear=2015&filterTransmission=Manual";
	public static final String norwegianVilnius = "https://cars.cartrawler.com/norwegian/en/book?clientID=242447&elID=0726201134239873913&countryID=LT&pickupID=3224&returnID=3224&pickupName=Vilnius%20Airport&returnName=Vilnius%20Airport&pickupDateTime=2015-09-01T10:00:00&returnDateTime=2015-09-02T10:00:00&age=30&curr=EUR&carGroupID=0&residenceID=LT&CT=AJ&referrer=0:&__utma=66135985.2092701990.1437977508.1437977508.1437977508.1&__utmb=66135985.3.10.1437977508&__utmc=66135985&__utmx=-&__utmz=66135985.1437977508.1.1.utmcsr&__utmv=-&__utmk=218255774#/vehicles";
	public static final String norwegianRiga = "https://cars.cartrawler.com/norwegian/en/book?clientID=139716&elID=0924211948108129703&countryID=LV&pickupID=4305&returnID=4305&pickupName=Riga%20-%20Airport&returnName=Riga%20-%20Airport&pickupDateTime=2015-10-01T10:00:00&returnDateTime=2015-11-02T10:00:00&age=30&curr=EUR&carGroupID=0&residenceID=LT&CT=AJ&referrer=0:&__utma=66135985.2092701990.1437977508.1437977508.1437977508.1&__utmb=66135985.3.10.1437977508&__utmc=66135985&__utmx=-&__utmz=66135985.1437977508.1.1.utmcsr&__utmv=-&__utmk=218255774#/vehicles";
	public static final String scannerVilnius = "http://cars-scanner.com/en/?_c=fnd.default&clientID=334782&elID=0807212554181994514&pickupID=3224&returnID=3224&pickupName=Vilnius%20Airport&returnName=Vilnius%20Airport&pickupDateTime=2015-11-01T10:00:00&returnDateTime=2015-11-02T10:00:00&age=30&curr=EUR&carGroupID=0&residenceID=LT&CT=AJ&referrer=0:&utm_source=google&utm_medium=poisk&utm_content=price_compare&utm_campaign=global_words&_c=fnd.default&gclid=CJTS9ojnl8cCFfHJtAodF8EANg#book";
	public static final String scannerRiga = "http://cars-scanner.com/en/?_c=fnd.default&clientID=334782&elID=0807212554181994514&pickupID=3224&returnID=3224&pickupName=Riga%20Airport&returnName=Riga%20Airport&pickupDateTime=2015-11-01T10:00:00&returnDateTime=2015-11-02T10:00:00&age=30&curr=EUR&carGroupID=0&residenceID=LT&CT=AJ&referrer=0:&utm_source=google&utm_medium=poisk&utm_content=price_compare&utm_campaign=global_words&_c=fnd.default&gclid=CJTS9ojnl8cCFfHJtAodF8EANg#book";
	public static int progress;
	public static Font font = new Font("Calibri", Font.BOLD, 15);
    public static final Color greenBg = new Color(197, 207, 215);
    public static final Color greenFg = new Color(0, 95, 145);
    public static final Color greenSelection = Color.white;
	
	public Sites() {
		super();
		cal = Calendar.getIntance();
//		readSiteNames();
//		setCategories();
	}

	public String getSiteName() {
		return this.siteName;
	}
	
	public void setSiteName(String str){
		this.siteName = str;
		pattern = str;
	}
	
	public void readSiteNames() {
			String sCurrentLine = null;
			
			FileReader fr = getFileReader(fileName);
			if(fr != null){
				try{
					
					BufferedReader br = new BufferedReader(fr);
					while((sCurrentLine = br.readLine()) != null){
						sites.add(sCurrentLine);
					}
//					System.out.println(sCurrentLine);
					setSiteName(sites.get(0));
					
				} catch (IOException e) {
					e.printStackTrace();
				}
				
			}else{
				System.out.println("-->FILE READER NULL readSiteNames");
			}
	}
	
	public String readFile(String params) {
		String sCurrentLine = null;
		
		FileReader fr = getFileReader(params);
		if(fr != null){
			try{
				String param = "";
				BufferedReader br = new BufferedReader(fr);
				while((sCurrentLine = br.readLine()) != null){
					param += sCurrentLine;
				}
				
//				System.out.println(sCurrentLine);
				return param;
				
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		}else{
			System.out.println("-->FILE READER NULL readFile");
		}
		return sCurrentLine;
		
}
		
	public ArrayList<String> getSiteNames(){
//		System.out.println("--> Number of sites found " + sNames.size());
		return sNames;
	}
	
	private String addZero(int num){
		if(num > 9){
			return "" + num;
		}else{
			return "0" + num;
		}
		
	}
	
	public String newDate(int year, int month, int day){

		//Norvegian Scanner
		String dateToChange = "returnDateTime=201(\\d)-\\d\\d-\\d\\d";
		String newDate = "returnDateTime=" + year + "-" + addZero(month) + "-" + addZero(day);
		this.siteName = this.siteName.replaceAll(dateToChange, newDate);
		
		//Rental Baltic
		this.siteName = this.siteName.replaceAll("doDay=[\\d]*", "doDay=" + addZero(day));
		this.siteName = this.siteName.replaceAll("doMonth=[\\d]*", "doMonth=" + addZero(month));
		System.out.println("--> Date Changed from:" + "to  " + day + "\n" + "NOW SITE IS: " + "\n" + this.siteName);
//		System.out.println(year + " " + month + " " + day);
		return this.siteName;
	}
	
	public String initDate(int year, int month, int day){
		
		/*Norvegian Scanner*/
		String dateToChange = "pickupDateTime=201(\\d)-\\d\\d-\\d\\d";
		String newDate = "pickupDateTime=" + year + "-" + addZero(month) + "-" + addZero(day);
		this.siteName = this.siteName.replaceAll(dateToChange, newDate);
		String dateToChange1 = "returnDateTime=201(\\d)-\\d\\d-\\d\\d";
		String newDate2 = "returnDateTime=" + year + "-" + addZero(month) + "-" + addZero(day);
		this.siteName = this.siteName.replaceAll(dateToChange1, newDate2);
		/*Rental Baltic 
		 * */
		this.siteName = this.siteName.replaceAll("puDay=(\\d)*", "puDay=" + addZero(day));
		this.siteName = this.siteName.replaceAll("puMonth=(\\d)*", "puMonth=" + addZero(month));
		this.siteName = this.siteName.replaceAll("puYear=(\\d)*", "puYear=" + year);
		System.out.println("--> Date Changed from:" + "to  " + day + "\n" + "initDateNOW SITE IS: " + "\n" + this.siteName);
		this.siteName = this.siteName.replaceAll("doDay=(\\d)*", "doDay=" + addZero(day));
		this.siteName = this.siteName.replaceAll("doMonth=(\\d)*", "doMonth=" + addZero(month));
		this.siteName = this.siteName.replaceAll("doYear=(\\d)*", "doYear=" + year);
		
//		System.out.println(year + " " + month + " " + day);
		return this.siteName;
	}
	
	public String setRiga(String site){
		/*
		*/
		String country = "&country=[\\w]+&";
		String newCountry = "&country=Latvia&";
		
		String dropCountry = "&dropCountry=[\\w]+&";
		String newDropCountry = "&dropCountry=Latvia&";
		
		String doLocName = "&dropLocationName=[\\w\\+]+&";
		String newDoLocationName = "&dropLocationName=Riga+Airport&";
		
		String locationName = "&locationName=[\\w\\+]+&";
		String newLocationName = "&locationName=Riga+Airport&";
		
		String doLocationName = "&dropLocation=\\d*&";
		String newDoLocName = "&dropLocation=1373298&";
		
		String location = "&location=[\\d]*&";
		String newLocation = "&location=1373298&";
		
		String dropCity = "\\?dropCity=[\\w^&]+&";
		String newDropCity = "\\?dropCity=Riga&";
		
		String city = "&city=[\\w]+&";
		String newCity = "&city=Riga&";
		
		
		site = site.replaceAll(country, newCountry);
		site = site.replaceAll(dropCountry, newDropCountry);
		site = site.replaceAll(doLocName, newDoLocName);
		site = site.replaceAll(doLocName, newDoLocationName);
		site = site.replaceAll(locationName, newLocationName);
		site = site.replaceAll(location, newLocation);
		site = site.replaceAll(dropCity, newDropCity);
		site = site.replaceAll(city, newCity);

		return site;
	}
	
	public void setCategories(){
		sNames = new ArrayList<String>();
		addEcoClass();
		addTransm();
		for(String str : sClasses){
			String newSite = siteName.replace("economy", str);
			sNames.add(newSite);
			//escape PWMR
			if(!str.toLowerCase().equals("carriers_9") && !str.toLowerCase().equals("mini")){
				newSite = newSite.replace("Manual", "Automatic");
				sNames.add(newSite);
			}
		}
	}
	
	public void addEcoClass(){
		String ecoClass = "&filter_carclass=economy";
		if(!siteName.contains(ecoClass)){
			siteName = siteName.concat(ecoClass);
		}
	}
	
	public void addTransm(){
		String transm = "&filterTransmission=Manual";
		if(!siteName.contains(transm)){
			siteName = siteName.concat(transm);
		}
	}
	
	public void resetSiteName(){
		siteName = pattern;
	}
	
	private FileReader getFileReader(String fileName){
		File f = new File(fileName);
		if(f.exists() && !f.isDirectory()) {
			try {
				return new FileReader(fileName);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}else{
			System.out.println("File not found");
		}
		return null;
	}
};
