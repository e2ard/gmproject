package Source;

import java.util.ArrayList;

public class Offer {
	private String supplier;
	private float price;
	private String GMsupplier;
	private float GMprice;
	private String CRsupplier;
	private float CRprice;
	private String site;
	
	public String getSupplier() {
		return supplier;
	}
	public void setSupplier(String supplier1) {
		if(supplier1 != null)
			supplier = supplier1;
	}
	
	public float getPrice() {
		return price;
	}
	public void setPrice(float minPrice) {
			price = minPrice;
	}
	
	public String getGMSupplier() {
		return GMsupplier;
	}
	public void setGMSupplier(String supplier) {
		GMsupplier = supplier;
	}
	
	public float getGMPrice() {
		return GMprice;
	}
	
	public void setGMPrice(float minPrice) {
		GMprice = minPrice;
	}
	
	public String getCRSupplier() {
		return CRsupplier;
	}
	public void setCRSupplier(String supplier) {
		CRsupplier = supplier;
	}
	
	public float getCRPrice() {
		return CRprice;
	}
	
	public void setCRPrice(float minPrice) {
		CRprice = minPrice;
	}
	
	public void setSite(String siteName) {
		site = siteName;
	}
	
	public String getSite() {
//		if(!(this.site == null))
		{
			return this.site;
//		}else{
//			return "--> getSite Nera";
		}
	}
	
	public Offer(float ownPrice, String supplier) {
		super();
		this.supplier = supplier;
		this.price = ownPrice;
	}
	public Offer(){};
	
	@Override
	public String toString() {
		if(!(supplier == null)){
			return supplier + " " + price + "\n";
		}else{
			return null;
		}
	}
	public String getOffer(){
		if(price < GMprice){
			return validate(supplier, price) + "\n" + validate(GMsupplier, GMprice);
		}else{
			return validate(GMsupplier, GMprice) + "\n" + validate(supplier, price);
		}
	}
	
	public String getOffer1(){
	       
        Offer other = new Offer(price, supplier);
        Offer GMoffer = new Offer(GMprice, GMsupplier);
        Offer CRother = new Offer(CRprice, CRsupplier);
        ArrayList<Offer> tempOffers = new ArrayList<Offer>();
//        if(!(other.getSupplier() == null))
        	tempOffers.add(other);
//        if(!(GMoffer.getSupplier() == null))
    		tempOffers.add(GMoffer);
//        if(!(CRother.getSupplier() == null))
        	tempOffers.add(CRother);
        String output = "";
        for(int j = 0; j < tempOffers.size(); j++){
            double min = tempOffers.get(j).getPrice();
            int index = j;
            for(int i = 0; i < tempOffers.size(); i++){
                if(tempOffers.get(i).getPrice() < min){
                    min = tempOffers.get(i).getPrice();
                    index = i;
                }
            }
            String supplier = tempOffers.get(index).getSupplier();
            float price = tempOffers.get(index).getPrice();
            output += validate(supplier, price) + "\n";
            tempOffers.remove(index);
        }
//        if(tempOffers.size() > 0){
	        String supplier = tempOffers.get(tempOffers.size()-1).getSupplier();
	        float price = tempOffers.get(tempOffers.size()-1).getPrice();
	        output += validate(supplier, price);
//        }
        return output;
    }
    
	private String validate(String s, float f){
		int strLen = 0;
		if(s != null){
			if(s.length() > 2)
				strLen = 2;
			else
				strLen = 2;
			return Math.round(f*100.0f)/100.0f + " " + s.toLowerCase().substring(0, strLen);
		}else{
			return "";
		}
	}
};
